import chunk
import os
import requests
import urllib.parse
from multiprocessing import Pool
from tqdm import tqdm

AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:49.0) Gecko/20100101 Firefox/49.0"
HEADERS = {"User-Agent": AGENT}

def getFileName(url):
    """
    ダウンロードリンクからファイル名だけを取得する。その際URLエンコードされていたらデコードする
    :param url: ダウンロードリンク
    :return: ファイル名
    """
    filename = urllib.parse.unquote(url.split("/")[-1])
    return(filename)

def download(url, savedir="/Users/Shady/Downloads/", *args, **kwargs):
    """
    urlからファイルをダウンロードする
    :param url: ダウンロード対象のURL
    :param savedir: ファイルを保存するフォルダ
    :return:
    """
    # print(kwargs["referer"])
    if "referer" in kwargs.keys():
        HEADERS["referer"] = kwargs["referer"]

    logger = kwargs["logger"] if "logger" in kwargs.keys() else None

    res = requests.get(url, stream=True, headers=HEADERS)
    if logger:
        logger.info(res.status_code)
    if res.status_code == requests.codes.ok: #200 in status_codes.py
        logger.info(res.headers)
        file_size = int(res.headers.get("content-length", 0))
        if "filename" in kwargs.keys():
            save_file_name = kwargs["filename"]
        else:
            save_file_name = getFileName(url)
        filename = os.path.join(savedir, save_file_name)
        if logger:
            logger.info("Downloading: %s", url)
        print("Download:", filename)
        pbar = tqdm(total=file_size, unit="B", unit_scale=True)
        with open(filename, "wb") as file:
            for chunk in res.iter_content(chunk_size=1024):
                file.write(chunk)
                pbar.update(len(chunk))
            pbar.close()
    else:
        print("Aborted. status code=", res.status_code)

def down(url, savedir="temp/"):
    download(url, savedir=savedir)

def downloads(links, savedir="/Users/Shady/Downloads/"):
    with Pool(processes=10) as pool:
        for i in pool.imap_unordered(down, links):
            pass



if __name__ == "__main__":
    # download("https://cdn1.www.st-hatena.com/users/zi/zipsan/profile.gif")
    # download("http://nfeed.net/AbemaTV/test/json.php", referer="http://nsdev.jp/memo/10149.html")
    # download("http://127.0.0.1:8000")
    download("http://nfeed.net/AbemaTV/test/json.php", referer="http://nsdev.jp/memo/10149.html", filename="abema.json")
