# -*- coding: utf-8 -*-

import json
import os
from datetime import datetime, timedelta, date
import time
from module.download import download

URL="http://nfeed.net/AbemaTV/test/json.php"
REFERER = "http://nsdev.jp/memo/10149.html"

def download_json_file():
    try:
        download(URL,
                 referer=REFERER,
                 filename=os.path.join(os.getcwd(), "file", "abema.json"))
    except:
        print("Download json file Error. Check the network connection.")
        return

def ameba_parse(path, hours=1, minutes=0):
    now_ = datetime.now()
    after = now_ + timedelta(hours=hours, minutes=minutes)
    if os.path.exists(path) is False:
        download_json_file()
    else:
        file_timestamp = os.path.getmtime(path)

        # タイムスタンプが当日でなければ取得
        today_timestamp = time.mktime(date.today().timetuple())
        if today_timestamp > file_timestamp:
            download_json_file()

    with open(path, "r", encoding="utf_8_sig") as fi:
        # ファイルから読み込み
        json_text = fi.readline()
        # 非直列化
        json_dict = json.loads(json_text)
        channel_list = json_dict["channels"]

        # アニメ専門チャンネルの辞書(ID: 名称)　
        # my_channel_dict = {channel["id"]:channel["name"] for channel
        #                       in channel_list if "anime" in channel["id"]}
        # my_channel_dict["documentary"] = "Documentaryチャンネル"
        # print("アニメ専門チャンネルの ID:名称　辞書", anime_channel_dict)

        my_channel_dict = {"documentary":  "Documentaryチャンネル",
                           "anime24": "アニメ24チャンネル",
                           "midnight-anime": "深夜アニメチャンネル",
                           "new-anime": "新作TVアニメチャンネル",
                           "fighting-sports":"格闘チャンネル",}

        program_dict_list = json_dict["channelSchedules"]
        anime_program_list = [name for name in program_dict_list
                              if name["channelId"] in my_channel_dict.keys()]
        # print(anime_program_list)

        for anime_program in anime_program_list:
            # print("チャネルID", anime_program["channelId"])
            for dict in my_channel_dict.keys():
                if dict == anime_program["channelId"]:
                    # print(dict)
                    pass
            programs = anime_program["slots"]

        print("-" * 80)
        schedules = json_dict["channelSchedules"]
        # now_ = datetime.now()
        # after = now_ + timedelta(hours=2)
        for schedule in schedules:
            if (schedule["channelId"] in my_channel_dict.keys()):
                program_dict_list = schedule["slots"]
                print("[", my_channel_dict[schedule["channelId"]], "]")
                for program_dict in program_dict_list:
                    title = program_dict["title"]
                    startAt = program_dict["startAt"]
                    start_dt = datetime.fromtimestamp(startAt)
                    endAt = program_dict["endAt"]
                    end_dt = datetime.fromtimestamp(endAt)

                    if endAt >= now_.timestamp() and startAt <= after.timestamp():
                        print(start_dt.strftime("%H:%M"), "〜", end_dt.strftime("%H:%M"), "{:<30}".format(title) )
        # ToDo 指定した時間以降3番組を表示する

if __name__ == "__main__":
    try:
        os.chdir(os.path.dirname(__file__))
    except:
        ameba_parse("file/abema.json")
    else:
        current_dir = os.curdir
        try:
            os.chdir(os.path.dirname(__file__))
            ameba_parse("file/abema.json")
        finally:
            os.chdir(current_dir)
